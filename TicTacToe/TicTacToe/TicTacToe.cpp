#include "pch.h"
#include <iostream>

char board[3][3] = { '1', '2', '3', '4', '5', '6', '7', '8', '9' };
char player = 'X';
const char PLAYERX = 'X';
const char PLAYERO = 'O';
void Drawing()                                                              //Drawing fields so players can see what they are choosing later, also added an system("cls") to make program more readable
{
	system("cls");
	std::cout << "TicTacToe game";
	for (int i = 0; i < 3; i++)
	{
		std::cout << "\n";
		for (int j = 0; j < 3; j++)
		{
			std::cout << board[i][j] << " ";
		}
	}
	std::cout << "\n";
}
void Input()																//Letting players to change fields
{
	std::cout << "Player " << player << " Choosing field: ";
	int inputboard;
	std::cin >> inputboard;
	//while (!(std::cin >> inputboard))										//this loop should validate the input of player, if player input character instead of a number, error message should appear with request to enter the number but it isn't working too well
	//{
	//	std::cin.clear();
	//	std::cin.ignore(1000, '\n');
	//	std::cout << "input a number!!!" << "\n";
	//}
	switch (inputboard)
	{
	case 1:
		if (board[0][0] != PLAYERX && board[0][0] != PLAYERO)
		{
			board[0][0] = player;
		}
		else
		{
			std::cout << "This field is already taken! Choose another one." << "\n";
			Input();
		}
		break;
	case 2:
		if (board[0][1] != PLAYERX && board[0][1] != PLAYERO)
		{
			board[0][1] = player;
		}
		else
		{
			std::cout << "This field is already taken! Choose another one." << "\n";
			Input();
		}
		break;
	case 3:
		if (board[0][2] != PLAYERX && board[0][2] != PLAYERO)
		{
			board[0][2] = player;
		}
		else
		{
			std::cout << "This field is already taken! Choose another one." << "\n";
			Input();
		}
		break;
	case 4:
		if (board[1][0] != PLAYERX && board[1][0] != PLAYERO)
		{
			board[1][0] = player;
		}
		else
		{
			std::cout << "This field is already taken! Choose another one." << "\n";
			Input();
		}
		break;
	case 5:
		if (board[1][1] != PLAYERX && board[1][1] != PLAYERO)
		{
			board[1][1] = player;
		}
		else
		{
			std::cout << "This field is already taken! Choose another one." << "\n";
			Input();
		}
		break;
	case 6:
		if (board[1][2] != PLAYERX && board[1][2] != PLAYERO)
		{
			board[1][2] = player;
		}
		else
		{
			std::cout << "This field is already taken! Choose another one." << "\n";
			Input();
		}
		break;
	case 7:
		if (board[2][0] != PLAYERX && board[2][0] != PLAYERO)
		{
			board[2][0] = player;
		}
		else
		{
			std::cout << "This field is already taken! Choose another one." << "\n";
			Input();
		}
		break;
	case 8:
		if (board[2][1] != PLAYERX && board[2][1] != PLAYERO)
		{
			board[2][1] = player;
		}
		else
		{
			std::cout << "This field is already taken! Choose another one." << "\n";
			Input();
		}
		break;
	case 9:
		if (board[2][2] != PLAYERX && board[2][2] != PLAYERO)
		{
			board[2][2] = player;
		}
		else
		{
			std::cout << "This field is already taken! Choose another one." << "\n";
			Input();
		}
		break;
	default:
		std::cout << "Wrong number! Take another one." << "\n";
		Input();
	}

}
void PlayerSwitch()																//If statement for switching player every turn
{
	if (player == PLAYERX)
	{
		player = PLAYERO;
	}
	else
	{
		player = PLAYERX;
	}
}
char CheckWin()																	//Checking if someone wins
{
	int count = 0;
	int row;
	int col;
	for (row = 0; row < 3; ++row)												//loops which checks rows and columns 
	{
		count = 0;
		for (col = 0; col < 3; ++col)
		{
			if (board[row][col] == PLAYERX)
			{
				count = count + 1;
			}
			else if (board[row][col] == PLAYERO)
			{
				count = count - 1;
			}
			if (count == 3 || count == -3)
			{
				count = count % 2;
				return count;
			}
		}
	}
	
	for (col = 0; col < 3; ++col)
	{
		count = 0;
		for (row = 0; row < 3; ++row)
		{
			if (board[row][col] == PLAYERX)
			{
				count = count + 1;
			}
			else if (board[row][col] == PLAYERO)
			{
				count = count - 1;
			}
			if (count == 3 || count == -3)
			{
				count = count % 2;
				return count;
			}
		}
	}
	
	count = 0;
	for (col = 0; col < 3; ++col)												//also loops for diagonals
	{
		if (board[col][col] == PLAYERX)
		{
			count = count + 1;
		}
		else if (board[col][col] == PLAYERO)
		{
			count = count - 1;
		}
	}
	if (count == 3 || count == -3)
	{
		count = count % 2;
		return count; 
	}
	
	count = 0;
	for (col = 0; col < 3; ++col)
	{
		if (board[col][2 - col] == PLAYERX)
		{
			count = count + 1;
		}
		else if (board[col][2 - col] == PLAYERO)
		{
			count = count - 1;
		}
	}
	if (count == 3 || count == -3)
	{
		count = count % 2;
		return count;
	}

	return 0;
}


int main()																		//main loop
{
	int n = 0;
	Drawing();
	while (1)
	{
		n++;
		Input();
		Drawing();
		char Win = CheckWin();
		if (Win == 1)
		{
			std::cout << "Player X wins!" << "\n";
			break;
		}
		else if (Win == -1)
		{
			std::cout << "Player O wins!" << "\n";
			break;
		}
		else if (Win == 0 && n == 9)
		{
			std::cout << "It's a draw!" << "\n";
			break;
		}
		PlayerSwitch();
	}
	system("pause");
	return 0;
}
